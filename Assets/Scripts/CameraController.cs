﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private new Rigidbody rigidbody;
    private ShipModel shipModel;
    public Vector3 Offset;
    private Transform parent;

    private void Start()
    {
        Offset = new Vector3(0, 2, -6f);
        parent = transform.parent;
        rigidbody = transform.parent.GetComponent<Rigidbody>();
        shipModel = transform.parent.GetComponent<ShipModel>();

        transform.localPosition = Offset;
        transform.localRotation = Quaternion.identity;
    }

    private static float InverseLerpUnclamped(float t, float a, float b)
        => (t - a) / (b - a);

    void LateUpdate()
    {
        var movementVector = parent.InverseTransformDirection(rigidbody.velocity);

        var m = Mathf.Atan(InverseLerpUnclamped(movementVector.magnitude, 0, shipModel.MaxForwardSpeed));

        var movementOffset = -movementVector.normalized * (m < 0.005 ? 0 : m);

        var turnOffset = parent.InverseTransformDirection(Vector3.Cross(rigidbody.angularVelocity, parent.forward)) * 0.5f;

        transform.localPosition = Offset + movementOffset + turnOffset;
    }
}
