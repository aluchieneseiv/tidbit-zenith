﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AIController : MonoBehaviour
{
    [Header("Constants")]
    [SerializeField]
    private float radius = 1;
    [SerializeField]
    private float maxCollisionDistance = 100;
    [SerializeField]
    private LayerMask mask;
    [SerializeField]
    private float minDistance = 25;
    [SerializeField]
    private float safeDistance = 100;
    [SerializeField]
    private float displacementPower = 10;

    [Header("Priorities")]
    [SerializeField]
    private float pursuitPriority = 1;
    [SerializeField]
    private float fleePriority = 2;
    [SerializeField]
    private float circleAroundPriority = 2;
    [SerializeField]
    private float obstacleAvoidancePriority = 4;

    private Transform target;
    private Rigidbody targetRB;
    private ShipModel targetSM;
    public Transform Target
    {
        set
        {
            target = value;
            targetRB = target.GetComponent<Rigidbody>();
            targetSM = target.GetComponent<ShipModel>();
        }
    }

    private ShipController shipController;
    private new Rigidbody rigidbody;
    private ShipModel shipModel;
    private LaserWeapon weapon;

    private float accelerationScale;
    private Vector3 rotationDir;
    private bool isShooting;

    private void Awake()
    {
        shipController = GetComponent<ShipController>();
        shipModel = GetComponent<ShipModel>();
        rigidbody = GetComponent<Rigidbody>();
        weapon = GetComponent<LaserWeapon>();
    }

    private void Start()
    {
        Target = GameMaster.Master.Player.transform;
        shipModel.OnKilled += delegate { Destroy(gameObject); };
    }

    private void FixedUpdate()
    {
        accelerationScale = 0;
        rotationDir = new Vector3();

        RaycastHit hit;
        if (DetectCollision(out hit))
            AvoidCollision(hit);

        if (target != null)
        {
            if (Vector3.Distance(transform.position, target.position) > minDistance)
                Seek();
            else
                CircleAround();

            if (weapon != null)
            {
                var a = Quaternion.LookRotation(transform.forward);
                var b = Quaternion.LookRotation(TargetShootDir().normalized);
                if (Quaternion.Angle(a, b) < 45)
                {
                    if (!isShooting)
                        StartShooting();
                    PointGun();
                }
                else
                    StopShooting();
            }
        }

        Displacement();

        shipController.Accelerate(Mathf.Clamp01(accelerationScale));
        shipController.Rotate(rotationDir.normalized);
    }

    private bool DetectCollision(out RaycastHit hit)
        => Physics.SphereCast(transform.position, radius, rigidbody.velocity.normalized, out hit, maxCollisionDistance, mask);

    private void AvoidCollision(RaycastHit hit)
    {
        accelerationScale += 1;

        Vector3 dir;
        try
        {
            var collider = hit.collider as MeshCollider;
            var vertexDistance = collider.sharedMesh.vertices.Select(x => Vector3.Distance(hit.point, hit.transform.TransformPoint(x))).ToArray();
            var normal = collider.sharedMesh.normals[System.Array.IndexOf(vertexDistance, vertexDistance.Min())];

            dir = transform.InverseTransformDirection(hit.transform.TransformDirection(normal));
        }
        catch
        {
            dir = transform.InverseTransformDirection(hit.transform.TransformDirection(hit.point - hit.collider.bounds.center));
        }

        var mask = hit.transform.gameObject.layer;
        rotationDir += Vector3.Cross(Vector3.forward, dir).normalized * (mask == LayerMask.NameToLayer("Default") ? obstacleAvoidancePriority : obstacleAvoidancePriority / 2);
    }

    Vector3 TargetInterceptCourse()
        => CalculateInterceptCourse(target.position, targetRB.velocity, transform.position, shipModel.MaxForwardSpeed);

    Vector3 TargetShootDir()
        => CalculateInterceptCourse(target.position, targetRB.velocity, transform.position, weapon.bulletSpeed);

    public static Vector3 CalculateInterceptCourse(Vector3 targetPos, Vector3 targetSpeed, Vector3 interceptorPos, float interceptorSpeed)
    {
        Vector3 targetDir = targetPos - interceptorPos;
        float iSpeed2 = interceptorSpeed * interceptorSpeed;
        float tSpeed2 = targetSpeed.sqrMagnitude;
        float fDot1 = Vector3.Dot(targetDir, targetSpeed);
        float targetDist2 = targetDir.sqrMagnitude;
        float d = (fDot1 * fDot1) - targetDist2 * (tSpeed2 - iSpeed2);
        if (d < 0.1f)  // negative == no possible course because the interceptor isn't fast enough
            return targetPos - interceptorPos;
        float sqrt = Mathf.Sqrt(d);
        float S1 = (-fDot1 - sqrt) / targetDist2;
        float S2 = (-fDot1 + sqrt) / targetDist2;
        if (S1 < 0.0001f)
        {
            if (S2 < 0.0001f)
                return Vector3.zero;
            else
                return (S2) * targetDir + targetSpeed;
        }
        else if (S2 < 0.0001f)
            return (S1) * targetDir + targetSpeed;
        else if (S1 < S2)
            return (S2) * targetDir + targetSpeed;
        else
            return (S1) * targetDir + targetSpeed;
    }

    void Seek()
    {
        var distance = Vector3.Distance(transform.position, target.position);
        var scale = Mathf.Lerp(0, 1, (distance - minDistance) / safeDistance);
        accelerationScale += scale;

        var desired_dir = transform.InverseTransformVector(TargetInterceptCourse().normalized);
        rotationDir += Vector3.Cross(Vector3.forward, desired_dir) * pursuitPriority;
    }

    void Flee()
    {
        accelerationScale += 1;

        var desired_dir = transform.InverseTransformVector((-TargetInterceptCourse()).normalized);
        rotationDir += Vector3.Cross(Vector3.forward, desired_dir) * pursuitPriority;
    }

    void CircleAround()
    {
        accelerationScale += 1;

        var circle_point = target.position + Random.onUnitSphere * safeDistance;
        var desired_dir = transform.InverseTransformVector((circle_point - transform.position).normalized);
        rotationDir += Vector3.Cross(Vector3.forward, desired_dir) * circleAroundPriority;
    }

    void Displacement()
    {
        var displacement_dir = rigidbody.velocity + Random.onUnitSphere * displacementPower;
        shipController.Strafe(displacement_dir.normalized);
    }

    void PointGun()
    {
        var dir = transform.InverseTransformVector(TargetShootDir().normalized);
        shipController.SetGunDirection(dir);
    }

    void StartShooting()
    {
        isShooting = true;
        shipController.StartShootingPrimary();
    }

    void StopShooting()
    {
        isShooting = false;
        shipController.StopShootingPrimary();
    }
}
