﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneProxy : MonoBehaviour
{
    public static GameObject Proxy => proxy.Value;
    private static Lazy<GameObject> proxy = new Lazy<GameObject>(() => GameObject.Find("SceneProxy"));

    public static bool ProxyExists
        => proxy.Value != null;

    public static SceneProxy ProxyScript => proxyScript.Value;
    private static Lazy<SceneProxy> proxyScript = new Lazy<SceneProxy>(() => proxy.Value?.GetComponent<SceneProxy>());

    public event Action OnSceneChanged;

    [SerializeField]
    private LoadingScreen loading;

    private void Start()
    {
        
    }

    public void ChangeScene(string name)
    {
        StartCoroutine(switchScene(name));
    }

    public void Store(GameObject go, string name)
    {
        go.transform.SetParent(transform, false);

        go.name = name;
    }

    public GameObject Extract(string name)
    {
        var t = transform.Find(name);

        if(t != null)
        {
            t.SetParent(null, false);
            return t.gameObject;
        }

        return null;
    }

    private IEnumerator switchScene(string name)
    {
        DontDestroyOnLoad(gameObject);

        loading.StartLoading();

        var load = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);

        while (load.progress < 0.9f)
        {
            loading.SetProgress(load.progress);
            yield return new WaitForFixedUpdate();
        }

        load.allowSceneActivation = true;

        yield return load;

        loading.StopLoading();

        SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());

        OnSceneChanged?.Invoke();
    }
}