﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferCurves : MonoBehaviour
{
    private float length;
    private float speed;
    private float scale;
    private float resolution;

    LineRenderer lineRenderer;
    float start;
    float step;
    int randomOffset;

    public void Init(float length, float speed, float scale, int resolution)
    {
        this.length = length;
        this.speed = speed;
        this.scale = scale;
        this.resolution = resolution;

        step = length / resolution;

        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = resolution;
    }

    private void FixedUpdate()
    {
        var backup = Random.state;

        start += speed;
        if (start > 1)
        {
            start -= Mathf.Floor(start);
            randomOffset += Mathf.FloorToInt(start) * 2;
        }

        var lastPoint = CalculateProceduralBezierPoint(start);
        int i = 0;
        for (float p = start; i < resolution; p += step, i++)
        {
            lineRenderer.SetPosition(i, lastPoint);
            var nextPoint = CalculateProceduralBezierPoint(p);
            lastPoint = nextPoint;
        }

        Random.state = backup;
    }

    Vector3 CalculateProceduralBezierPoint(float i)
    {

        var prevInt = Mathf.FloorToInt(i);
        var nextInt = Mathf.CeilToInt(i);
        var t = i - prevInt;

        int prevEndPoint = prevInt * 2;
        int nextEndPoint = nextInt * 2;

        int prevControl = prevEndPoint - 1;
        int nextControl = nextEndPoint - 1;

        // find the control points
        Vector3 p0 = CalculateProceduralControlPoint(prevEndPoint);
        Vector3 p1 = p0 - (CalculateProceduralControlPoint(prevControl) - p0);
        Vector3 p2 = CalculateProceduralControlPoint(nextControl);
        Vector3 p3 = CalculateProceduralControlPoint(nextEndPoint);

        return CalculateBezierPoint(t, p0, p1, p2, p3);
    }

    Vector3 CalculateProceduralControlPoint(int controlPointNumber)
    {
        Random.InitState(controlPointNumber + randomOffset);
        var val1 = (Random.value * 2) - 1;
        var val2 = Random.value - 0.5f;

        return new Vector3(val1 * scale, val2 * scale, controlPointNumber / 2.0f);
    }

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {

        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0; // first term
        p += 3 * uu * t * p1; // second term
        p += 3 * u * tt * p2; // third term
        p += ttt * p3;        // fourth term

        return p;
    }
}
