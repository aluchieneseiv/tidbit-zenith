﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

namespace Generation
{
    public class Generator : MonoBehaviour
    {
        [SerializeField]
        private float objectScale = 25;
        [SerializeField]
        private int numberOfObjects = 200;
        [SerializeField]
        private int maxIterations = 100;

        List<Object> objects;
        public GameObject objectPrefab;
        public GameObject transferCurvePrefab;
        public GameObject boundaryPrefab;
        public Material[] materials;
        public GameObject[] enemies;
        private Material material;
        System.Type[] objectTypes = new[] { typeof(Sphere), typeof(Torus), typeof(PulsingSphere), typeof(PulsingTorus), typeof(Cylinder) };

        private float distanceMultiplier = 1.25f;
        Color objectColor;
        Color emissionColor;

        private void Start()
        {
            Generate();
        }

        public void Generate()
        {
            foreach (Transform t in transform)
                Destroy(t.gameObject);
            objects = new List<Object>();

            var b = Instantiate(boundaryPrefab, transform, true);
            b.transform.localScale = Vector3.one * 20000;

            objectColor = Random.ColorHSV(0, 0.7f, 0.9f, 1f, 0.1f, 0.6f);
            emissionColor = Random.ColorHSV(0, 1, 0.4f, 0.7f, 0.5f, 0.9f);
            PoolObjects();

            GeneratePositions();

            if (GameMaster.Master != null)
            {
                b.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);
                GameMaster.Master.OnLevelCompleted += delegate { b.GetComponent<Boundary>().LevelCompleted(); };
                GameMaster.Master.SetFileStats(0, (uint)objects.Count);
            }

            print(objects.Count);
        }

        Color GenerateLike(Color c)
           => new Color(c.r + Random.Range(-0.2f, 0.2f), c.g + Random.Range(-0.2f, 0.2f), c.b + Random.Range(-0.2f, 0.2f));

        void PoolObjects()
        {
            for (int i = 0; i < numberOfObjects; i++)
            {
                material = new Material(materials[Random.Range(0, materials.Length)]);

                var go = Instantiate(objectPrefab);
                go.name = $"Object {i}";
                go.transform.SetParent(transform);
                go.transform.localScale = Vector3.one * objectScale;
                go.transform.rotation = Random.rotation;

                material.SetColor("_TexColor", GenerateLike(objectColor));
                material.SetColor("_GlowColor", GenerateLike(emissionColor));
                material.SetFloat("_GlowPower", Random.Range(2.5f, 5.5f));
                material.SetColor("_RimColor", Color.black);
                material.SetFloat("_RimPower", Random.Range(1f, 4f));
                material.SetFloat("_TextureMovement", Random.Range(0, 2));
                material.SetFloat("_TextureSpeed", Random.Range(0.2f, 2));
                var r = go.GetComponent<MeshRenderer>();
                r.material = material;

                var obj = go.AddComponent(objectTypes[Random.Range(0, 5)]) as Object;
                obj.Generate();

                go.transform.position = Random.onUnitSphere;
                go.transform.position *= Random.Range(0, 10000 - obj.Bounds.extents.magnitude);

                int chance = Random.Range(0, 6);
                if (chance == 0)
                {
                    obj.threatModel = new Object.ThreatModel { prefab = enemies[Random.Range(0, enemies.Length)], count = Random.Range(1, 4) };
                }

                objects.Add(obj);

                go.GetComponent<MeshCollider>().sharedMesh = obj.mesh;
            }
        }

        private void GeneratePositions()
        {
            for(int i = 0; i < objects.Count; i++)
            {
                if (IsOk(objects[i]))
                {
                    float chance = Random.Range(0, 10);
                    if (chance == 0)
                        GenerateTransferCurve(objects[i].transform.position);
                    continue;
                }
                else
                {
                    for (int k = 0; k < maxIterations; k++)
                    {
                        objects[i].transform.position = Random.onUnitSphere * Random.Range(0, 10000);
                        if (IsOk(objects[i]))
                            break;
                    }
                    if (!IsOk(objects[i]))
                    {
                        Destroy(objects[i].gameObject);
                        objects.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        private void GenerateTransferCurve(Vector3 position)
        {
            var go = Instantiate(transferCurvePrefab, transform);
            go.transform.position = position;

            Vector3 endPoint = Random.onUnitSphere * 10000;
            Vector3 dir = endPoint - position;
            float length = dir.magnitude;
            go.transform.rotation = Quaternion.LookRotation(dir);
            go.GetComponent<TransferCurves>().Init(length, Random.Range(0.001f, 0.01f), Random.Range(50, 100), 100);
        }

        bool IsOk(Object o)
        {
            for (int j = 0; j < objects.Count; j++)
                if (o.name != objects[j].name && o.Bounds.Intersects(objects[j].Bounds))
                    return false;
            return true;
        }
    }
}
