﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{

    public class PulsingTorus : Torus
    {
        struct Coefficients
        {
            public float ro1;
            public float ro2;
            public float a;
            public float b;
            public float x;
            public float y;
            public float pstep;

            public Coefficients(int resolution)
            {
                ro1 = Random.Range(5.4f, 12.5f);
                ro2 = Random.Range(1.35f, 2.6f);
                a = Random.Range(1, 5);
                b = Random.Range(5, 10);
                x = SineRandom(0.05f, 0.15f);
                y = SineRandom(0.05f, 0.15f);
                pstep = 2f / resolution;
            }

            private static float SineRandom(float min, float max)
            {
                var f = Random.Range(0f, 2f);
                var biased = Mathf.Acos(1 - f) / Mathf.PI;

                return Mathf.Lerp(min, max, biased);
            }

        }

        Coefficients coef;

        public override void Generate()
        {
            Reset();

            resolution = Random.Range(15, 30);
            coef = new Coefficients(resolution);

            float t = Time.time;
            for (int i = 0, z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++, i++)
                    vertices.Add(PulsingTorusFunction(x, z, t));

            RecalculateTriangles();

            DetailCoefficients detailC = new DetailCoefficients
            {
                stellateChance = Random.Range(1, 5),
                stellateHeight = Random.Range(0.1f, 0.35f),
                greebleChance = Random.Range(1, 5),
                greebleLengthMin = Random.Range(0.1f, 0.2f),
                greebleLengthMax = Random.Range(0.25f, 0.5f)
            };

            int detailChance = Random.Range(0, 3);
            switch(detailChance)
            {
                case 0:
                    Greeble(detailC.greebleChance, detailC.greebleLengthMin, detailC.greebleLengthMax);
                    break;
                case 1:
                    Stellate(detailC.stellateChance, detailC.stellateHeight);
                    break;
                default:
                    break;
            }

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        Vector3 PulsingTorusFunction(float u, float v, float t)
        {
            float theta = (u + 0.5f) * coef.pstep - 1f;
            float phi = (v + 0.5f) * coef.pstep - 1f;
            float r1 = coef.ro1 + Mathf.Sin(Mathf.PI * (coef.a * theta + t)) * coef.x;
            float r2 = coef.ro2 + Mathf.Sin(Mathf.PI * (coef.b * phi + t)) * coef.y;
            float s = r2 * Mathf.Cos(Mathf.PI * phi) + r1;
            return new Vector3
            {
                x = s * Mathf.Sin(Mathf.PI * theta),
                y = r2 * Mathf.Sin(Mathf.PI * phi),
                z = s * Mathf.Cos(Mathf.PI * theta)
            };
        }
    }
}
