﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public class Torus : Cylinder
    {
        struct Coefficients
        {
            public float outerR;
            public float innerR;
            public float step;

            public Coefficients(int resolution)
            {
                outerR = SineRandom(10, 15);
                innerR = SineRandom(1f, 4f);
                step = 2 * Mathf.PI / resolution;
            }

            private static float SineRandom(float min, float max)
            {
                var f = Random.Range(0f, 2f);
                var biased = Mathf.Acos(1 - f) / Mathf.PI;

                return Mathf.Lerp(min, max, biased);
            }

        }

        Coefficients coef;

        public override void Generate()
        {
            Reset();

            resolution = Random.Range(3, 25);
            coef = new Coefficients(resolution);

            float t = Time.time;
            for (int i = 0, z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++, i++)
                    vertices.Add(TorusFunction(x, z));

            RecalculateTriangles();

            DetailCoefficients detailC = new DetailCoefficients
            {
                stellateChance = Random.Range(1, 5),
                stellateHeight = Random.Range(0.1f, 0.35f),
                greebleChance = Random.Range(1, 5),
                greebleLengthMin = Random.Range(0.1f, 0.2f),
                greebleLengthMax = Random.Range(0.25f, 0.5f)
            };

            int detailChance = Random.Range(0, 3);
            switch (detailChance)
            {
                case 0:
                    Greeble(detailC.greebleChance, detailC.greebleLengthMin, detailC.greebleLengthMax);
                    break;
                case 1:
                    Stellate(detailC.stellateChance, detailC.stellateHeight);
                    break;
                default:
                    break;
            }

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        Vector3 TorusFunction(float u, float v)
        {
            float theta = u * coef.step;
            float phi = v * coef.step;
            return new Vector3
            {
                x = Mathf.Cos(theta) * (coef.outerR + Mathf.Cos(phi) * coef.innerR),
                y = Mathf.Sin(theta) * (coef.outerR + Mathf.Cos(phi) * coef.innerR),
                z = Mathf.Sin(phi) * coef.innerR
            };
        }
    }
}
