﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Generation
{
    public class Object : MonoBehaviour
    {
        protected struct Poly
        {
            public int[] verticesIndices;
            public int[] trisIndices;

            public Poly(int[] verticesIndices, int[] trisIndices)
            {
                this.verticesIndices = verticesIndices;
                this.trisIndices = trisIndices;
            }
        }

        public struct ThreatModel
        {
            public GameObject prefab;
            public int count;
        }

        public ThreatModel? threatModel;
        private bool scanned;
        public bool Scanned
        {
            get
            {
                return scanned;
            }
            set
            {
                scanned = value;
                if (scanned)
                    meshRenderer.material.SetColor("_RimColor", Color.white);
            }
        }

        protected MeshFilter meshFilter;
        protected MeshRenderer meshRenderer;
        public Mesh mesh;
        protected List<Vector3> vertices;
        protected List<int> triangles;
        protected List<Poly> polys;

        public Bounds Bounds => meshRenderer.bounds;

        protected void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            mesh = new Mesh();
            meshFilter.mesh = mesh;
        }

        public virtual void Generate() { }

        protected void Reset()
        {
            mesh.Clear();

            triangles = new List<int>();
            vertices = new List<Vector3>();
            polys = new List<Poly>();
        }

        protected void AddQuad(int i1, int i2, int i3, int i4)
        {
            int trisIndexStart = triangles.Count;
            AddTriangle(i1, i2, i3);
            AddTriangle(i1, i3, i4);
            polys.Add(new Poly(new[]{ i1, i2, i3, i4 }, Enumerable.Range(trisIndexStart, 6).ToArray()));
        }

        protected void AddTriangle(int i1, int i2, int i3)
        {
            triangles.Add(i1);
            triangles.Add(i2);
            triangles.Add(i3);
        }
        protected Vector3 GetPlaneNormal(int[] poly)
        {
            Vector3 e1 = vertices[poly[1]] - vertices[poly[0]];
            Vector3 e2 = vertices[poly[2]] - vertices[poly[1]];

            return Vector3.Cross(e1, e2).normalized;
        }
    }
}
