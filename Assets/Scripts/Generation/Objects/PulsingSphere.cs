﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public class PulsingSphere : Sphere
    {
        struct Coefficients
        {
            public float ro;
            public float a;
            public float b;
            public float x;
            public float y;
            public float scale;
            public float pstep;

            public Coefficients(int resolution)
            {
                ro = Random.Range(0.5f, 2f);
                a = SineRandom(1, 5);
                b = SineRandom(5, 10);
                x = Random.Range(0.05f, 0.15f);
                y = Random.Range(0.05f, 0.15f);
                scale = Random.Range(10f, 20f);
                pstep = 2f / resolution;
            }

            private static float SineRandom(float min, float max)
            {
                var f = Random.Range(0f, 2f);
                var biased = Mathf.Acos(1 - f) / Mathf.PI;

                return Mathf.Lerp(min, max, biased);
            }

        }

        Coefficients coef;

        public override void Generate()
        {
            Reset();

            resolution = Random.Range(10, 30);
            coef = new Coefficients(resolution);

            float t = Time.time;
            for (int i = 0, z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++, i++)
                    vertices.Add(PulsingSphereFunction(x, z, t));

            float height = (coef.ro + coef.x + coef.y) * coef.scale;
            vertices.Add(new Vector3(0, -height, 0));
            vertices.Add(new Vector3(0, height, 0));

            RecalculateTriangles();

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        Vector3 PulsingSphereFunction(float u, float v, float t)
        {
            float theta = (u + 0.5f) * coef.pstep - 1f;
            float phi = (v + 0.5f) * coef.pstep - 1f;
            float r = coef.ro + Mathf.Sin(Mathf.PI * (coef.a * theta + t)) * coef.x +
                                Mathf.Sin(Mathf.PI * (coef.b * phi + t)) * coef.y;
            r *= coef.scale;
            float s = r * Mathf.Cos(Mathf.PI * 0.5f * phi);
            return new Vector3
            {
                x = s * Mathf.Sin(Mathf.PI * theta),
                y = r * Mathf.Sin(Mathf.PI * 0.5f * phi),
                z = s * Mathf.Cos(Mathf.PI * theta)
            };
        }
    }
}
