﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public class Cylinder : DetailedObject
    {
        struct Coefficients
        {
            public float radius;
            public float thicknes;
            public float length;
            public float step;

            public Coefficients(int resolution)
            {
                radius = Random.Range(5, 10);
                thicknes = Random.Range(1.1f, 1.25f);
                length = SineRandom(11f, 17f) / resolution * 2;
                step = 2 * Mathf.PI / resolution;
            }

            private static float SineRandom(float min, float max)
            {
                var f = Random.Range(0f, 2f);
                var biased = Mathf.Acos(1 - f) / Mathf.PI;

                return Mathf.Lerp(min, max, biased);
            }

        }

        protected int resolution = 10;
        Coefficients coef;

        public override void Generate()
        {
            Reset();

            resolution = Random.Range(3, 15);
            coef = new Coefficients(resolution);

            float t = Time.time;
            for (int z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++)
                    vertices.Add(CylinderFunction(x, z));

            RecalculateTriangles();

            DetailCoefficients detailC = new DetailCoefficients
            {
                stellateChance = Random.Range(1, 5),
                stellateHeight = Random.Range(0.5f, 0.7f),
            };

            int detailChance = Random.Range(0, 2);
            switch (detailChance)
            {
                case 0:
                    Stellate(detailC.stellateChance, detailC.stellateHeight);
                    break;
                default:
                    break;
            }

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        protected void RecalculateTriangles()
        {
            int i1, i2, i3, i4;
            for (int z = 0; z < resolution; z++)
            {
                for (int x = 0; x < resolution; x++)
                {
                    i1 = z * resolution + x;
                    i2 = z * resolution + (x + 1 == resolution ? 0 : x + 1);
                    i3 = (z + 1 == resolution ? 0 : z + 1) * resolution + (x + 1 == resolution ? 0 : x + 1);
                    i4 = (z + 1 == resolution ? 0 : z + 1) * resolution + x;

                    AddQuad(i1, i2, i3, i4);
                }
            }
        }

        Vector3 CylinderFunction(float u, float v)
        {
            float theta = u * coef.step;
            float rev = (v > resolution / 2 ? 0 : 1);
            return new Vector3
            {
                x = Mathf.Cos(theta) * coef.radius * (rev == 1 ? rev * coef.thicknes : 1),
                y = Mathf.Sin(theta) * coef.radius * (rev == 1 ? rev * coef.thicknes : 1),
                z = (rev == 0 ? (resolution - v) * coef.length : v * coef.length)
            };
        }
    }
}
