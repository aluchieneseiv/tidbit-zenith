﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public class Sphere : DetailedObject
    {
        struct Coefficients
        {
            public float length;
            public float width;
            public float height;
            public float step;

            public Coefficients(int resolution)
            {
                length = SineRandom(7.5f, 12.5f);
                width = SineRandom(7.5f, 12.5f);
                height = SineRandom(7.5f, 12.5f);
                step = 2 * Mathf.PI / resolution;
            }

            private static float SineRandom(float min, float max)
            {
                var f = Random.Range(0f, 2f);
                var biased = Mathf.Acos(1 - f) / Mathf.PI;

                return Mathf.Lerp(min, max, biased);
            }

        }

        protected int resolution = 10;
        Coefficients coef;

        public override void Generate()
        {
            Reset();

            resolution = Random.Range(3, 15);
            coef = new Coefficients(resolution);

            float t = Time.time;
            for (int i = 0, z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++, i++)
                    vertices.Add(SphereFunction(x, z));

            vertices.Add(new Vector3(0, coef.height, 0));
            vertices.Add(new Vector3(0, -coef.height, 0));

            RecalculateTriangles();

            DetailCoefficients detailC = new DetailCoefficients
            {
                stellateChance = Random.Range(1, 5),
                stellateHeight = Random.Range(0.1f, 0.35f),
                greebleChance = Random.Range(1, 5),
                greebleLengthMin = Random.Range(0.05f, 0.15f),
                greebleLengthMax = Random.Range(0.2f, 0.35f)
            };

            int detailChance = Random.Range(0, 5);
            if (resolution < 10)
                detailChance = 5;
            if(detailChance < 1)
                Greeble(detailC.greebleChance, detailC.greebleLengthMin, detailC.greebleLengthMax);
            else if (detailChance < 4)
                Stellate(detailC.stellateChance, detailC.stellateHeight);

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        protected void RecalculateTriangles()
        {
            int i1, i2, i3, i4;

            for (int z = 0; z < resolution; z++)
                for (int x = 0; x < resolution; x++)
                {
                    i1 = z * resolution + x;
                    i2 = z * resolution + (x + 1 == resolution ? 0 : x + 1);
                    i3 = (z + 1) * resolution + (x + 1 == resolution ? 0 : x + 1);
                    i4 = (z + 1) * resolution + x;

                    if (z == 0)
                        AddTriangle(resolution * resolution, resolution + i2, resolution + i1);
                    else if (z + 1 == resolution)
                        AddTriangle(resolution * resolution + 1, i1, i2);
                    else
                        AddQuad(i1, i2, i3, i4);

                }
        }

        Vector3 SphereFunction(float u, float v)
        {
            float theta = coef.step / 2;
            float phi = coef.step;
            float r1 = Mathf.Sin(theta * v) * coef.width;
            float r2 = Mathf.Sin(theta * v) * coef.length;
            return new Vector3
            {
                x = Mathf.Cos(phi * u) * r1,
                y = Mathf.Cos(theta * v) * coef.height,
                z = Mathf.Sin(phi * u) * r2
            };
        }
    }
}
