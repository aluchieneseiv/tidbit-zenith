﻿using UnityEngine;

namespace Generation
{
    public class DetailedObject : Object
    {
        protected struct DetailCoefficients
        {
            public float greebleChance;
            public float greebleLengthMin;
            public float greebleLengthMax;
            public float stellateChance;
            public float stellateHeight;
        }

        protected void ExtrudePoly(int polyIndex, float length)
        {
            int[] poly = polys[polyIndex].verticesIndices;
            int[] newPoly = new int[poly.Length];
            Vector3 normal = GetPlaneNormal(poly);
            int endIndex = vertices.Count;

            for (int i = 0; i < poly.Length; i++)
            {
                Vector3 v = vertices[poly[i]];
                newPoly[i] = endIndex + i;
                vertices.Add(v + normal * length);
            }

            for (int i = 0; i < poly.Length; i++)
            {
                int j = (i + 1 == poly.Length ? 0 : i + 1);
                AddQuad(poly[i], poly[j], newPoly[j], newPoly[i]);
            }

            int[] trisIndices = polys[polyIndex].trisIndices;
            triangles[trisIndices[0]] = newPoly[0];
            triangles[trisIndices[1]] = newPoly[1];
            triangles[trisIndices[2]] = newPoly[2];

            triangles[trisIndices[3]] = newPoly[0];
            triangles[trisIndices[4]] = newPoly[2];
            triangles[trisIndices[5]] = newPoly[3];

            polys[polyIndex] = new Poly(newPoly, trisIndices);
        }

        protected void Greeble(float chance, float lengthMin, float lengthMax)
        {
            int count = polys.Count;
            for (int i = 0; i < count; i++)
            {
                float apply = Random.Range(0f, chance);
                if (apply < 1)
                {
                    float length = Random.Range(lengthMin, lengthMax);
                    ExtrudePoly(i, length);
                }
            }
        }

        protected void MakePyramid(int triIndex, float height)
        {
            int i1 = triangles[triIndex];
            int i2 = triangles[triIndex + 1];
            int i3 = triangles[triIndex + 2];

            Vector3 v1 = vertices[i1];
            Vector3 v2 = vertices[i2];
            Vector3 v3 = vertices[i3];

            Vector3 center = new Vector3()
            {
                x = (v1.x + v2.x + v3.x) / 3,
                y = (v1.y + v2.y + v3.y) / 3,
                z = (v1.z + v2.z + v3.z) / 3
            };

            Vector3 normal = GetPlaneNormal(new[] { i1, i2, i3 });

            int vi = vertices.Count;
            vertices.Add(center + (normal * height));

            triangles.RemoveAt(triIndex);
            triangles.RemoveAt(triIndex);
            triangles.RemoveAt(triIndex);

            AddTriangle(vi, i1, i2);
            AddTriangle(vi, i2, i3);
            AddTriangle(vi, i3, i1);
        }

        protected void Stellate(float chance, float height)
        {
            int count = triangles.Count;
            int j = 0;
            for (int i = 0; i < count; i += 3)
            {
                float apply = Random.Range(0f, chance);
                if (apply < 1)
                    MakePyramid(j, height);
                else
                    j += 3;
            }
        }
    }
}
