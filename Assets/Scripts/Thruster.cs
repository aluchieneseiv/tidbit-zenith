﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour
{
    [SerializeField]
    private Material thrusterMaterial;
    private List<Material> materials;

    private void Awake()
    {
        materials = new List<Material>();
        foreach (Transform c in transform)
        {
            var m = new Material(thrusterMaterial);
            c.GetComponent<MeshRenderer>().material = m;
            materials.Add(m);
        }
    }

    void Update ()
    {
        foreach(Transform c in transform)
            c.Rotate(0, 0, Random.Range(25f, 35f));
	}

    public void Scale(float scale)
    {
        foreach (var m in materials)
            m.SetFloat("_Scale", scale);
    }
}
