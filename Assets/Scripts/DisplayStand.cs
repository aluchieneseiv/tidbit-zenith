﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayStand : MonoBehaviour
{
    [Header("Display Settings")]
    public GameObject Object;
    public float Rotation = 15f;
    public Vector3 Axis = Vector3.up;

    public void SetDisplayModel(GameObject obj)
    {
        Object = obj;
        Object.transform.SetParent(gameObject.transform);
        Object.transform.localPosition = Vector3.zero;
        Object.transform.localRotation = Quaternion.identity;
    }

    private void FixedUpdate()
    {
        Object.transform.rotation *= Quaternion.AngleAxis(Rotation * Time.fixedDeltaTime, Axis);
    }
}
