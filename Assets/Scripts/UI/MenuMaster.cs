﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMaster : MonoBehaviour
{
    public static MenuMaster Master { get; private set; }

    [Header("General")]
    public float TransitionTime;
    public Camera Camera;
    public DisplayStand DisplayStand;

    public GameObject[] playerPrefabs;
    public GameObject mapPrefab;

    public UnityEngine.UI.Button continueButton;

    [Header("Menus")]
    public Menu MainMenu;
    public Menu UpgradeMenu;

    private SceneProxy sceneProxy;

    private GameObject map;
    private GameObject player;
    public GameObject Player
    {
        get
        {
            return player;
        }

        set
        {
            DisplayStand.SetDisplayModel(value);
            Destroy(player);
            player = value;
        }
    }

    private void Awake()
    {
        Master = this;
    }

    void Start()
    {
        sceneProxy = GameObject.Find("SceneProxy").GetComponent<SceneProxy>();

        MainMenu.gameObject.SetActive(true);
        UpgradeMenu.gameObject.SetActive(false);

        Camera.transform.SetPositionAndRotation(MainMenu.CameraPosition, MainMenu.CameraRotation);

        if (!false) // savefile doesn't exist
        {
            map = Instantiate(mapPrefab);

            continueButton.interactable = false;
        }
    }

    public void StartNewGame()
    {
        Player.GetComponent<ShipModel>().ResetMultipliers();

        startGame();
    }

    public void Continue()
    {
        startGame();
    }

    private void startGame()
    {
        MainMenu.gameObject.SetActive(false);
        UpgradeMenu.gameObject.SetActive(false);

        sceneProxy.Store(player, "Player");
        sceneProxy.OnSceneChanged += delegate { Instantiate(mapPrefab, Vector3.zero, Quaternion.identity); };

        sceneProxy.ChangeScene("Game");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void GoToSetupScreen()
        => StartCoroutine(TransitionMenu(MainMenu, UpgradeMenu));

    public void ReturnToMainMenu()
        => StartCoroutine(TransitionMenu(UpgradeMenu, MainMenu));

    private IEnumerator TransitionMenu(Menu from, Menu to)
    {
        from.gameObject.SetActive(false);

        var fromRotation = from.CameraRotation;
        var toRotation = to.CameraRotation;

        for (float time = 0; time < TransitionTime; time += Time.fixedDeltaTime)
        {
            var t = time / TransitionTime;

            var pos = Vector3.Lerp(from.CameraPosition, to.CameraPosition, t);
            var rot = Quaternion.Slerp(fromRotation, toRotation, t);

            Camera.transform.SetPositionAndRotation(pos, rot);
            yield return new WaitForFixedUpdate();
        }

        Camera.transform.SetPositionAndRotation(to.CameraPosition, toRotation);

        to.gameObject.SetActive(true);
    }
}
