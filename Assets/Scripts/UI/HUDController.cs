﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.UI.Text announcement;
    [SerializeField]
    private Transform crosshair;
    [SerializeField]
    private BarController healthBar;
    [SerializeField]
    private BarController energyBar;
    [SerializeField]
    private BarController scanningBar;
    [SerializeField]
    [Range(0f, 1f)]
    private float maxAimRadius = 0.75f;
    [SerializeField]
    private float aimSensitivity = 1f;
    [SerializeField]
    private GameObject enemyPrefab;

    private readonly float aimRadius = 1080 / 2f;

    public Vector3 NormalizedAim { get; private set; }

    public Vector3 CrosshairPos
        => transform.TransformPoint(NormalizedAim.y * aimRadius * maxAimRadius, -NormalizedAim.x * aimRadius * maxAimRadius, 0);

    public void UpdateHealth(float amount)
        => healthBar.Fill(amount);

    public void UpdateEnergy(float amount)
        => energyBar.Fill(amount);

    public void SetScannerActive(bool b)
        => scanningBar.gameObject.SetActive(b);

    public void SetScanner(float amount)
        => scanningBar.Fill(amount);

    public void AddEnemy(GameObject enemy)
    {
        var ui = Instantiate(enemyPrefab, gameObject.transform).GetComponent<UIEnemy>();

        ui.Target = enemy;
    }

    public bool ClampToCircle(ref Vector3 screenPos)
    {
        if(screenPos.magnitude > aimRadius * maxAimRadius)
        {
            screenPos = CircleEdge(screenPos);
            return false;
        }

        return true;
    }

    public Vector3 CircleEdge(Vector3 dir)
        => dir.normalized * aimRadius * maxAimRadius;

    private void OnEnable()
    {
        announcement.text = string.Empty;
    }

    private void Update()
    {
        NormalizedAim = Vector3.ClampMagnitude(NormalizedAim + new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X")) * aimSensitivity, 1f);
        crosshair.position = CrosshairPos;
    }

    public void Announce(string text, float speed, float time)
        => StartCoroutine(announceCoroutine(text, speed, time));

    private IEnumerator announceCoroutine(string text, float speed, float time)
    {
        for (int i = 1; i <= text.Length; i++)
        {
            announcement.text = text.Substring(0, i);
            yield return new WaitForSecondsRealtime(1 / speed);
        }
        yield return new WaitForSecondsRealtime(time);

        announcement.text = string.Empty;
    }

}
