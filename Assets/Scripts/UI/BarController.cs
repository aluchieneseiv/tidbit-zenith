﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarController : MonoBehaviour
{
    [Header("Image")]
    [SerializeField]
    private UnityEngine.UI.Image image;
    [SerializeField]
    private float minimumFill;
    [SerializeField]
    private float maximumFill;

    [Header("Text")]
    [SerializeField]
    private UnityEngine.UI.Text text;
    [SerializeField]
    private string format;

    public void Fill(float amount, float value = 0)
    {
        image.fillAmount = Mathf.Lerp(minimumFill, maximumFill, amount);

        if(text != null)
            text.text = string.Format(format, Mathf.Round(amount * 100), Mathf.Round(value));
    }
}
