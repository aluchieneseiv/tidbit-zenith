﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static MenuMaster;

public class UpgradeMaster : MonoBehaviour
{
    [SerializeField]
    private RectTransform shipHealth;
    [SerializeField]
    private RectTransform shipSpeed;
    [SerializeField]
    private RectTransform shipEnergy;
    [SerializeField]
    private RectTransform laserDamage;
    [SerializeField]
    private RectTransform laserFireRate;
    [SerializeField]
    private RectTransform rocketDamage;
    [SerializeField]
    private RectTransform rocketFireRate;
    [SerializeField]
    private RectTransform rocketRadius;
    [SerializeField]
    private RectTransform rocketAgility;
    [SerializeField]
    private RectTransform rocketLifeTime;

    RectTransform[] upgrades;

    Button[] minusButtons;
    Button[] plusButtons;
    Image[] bars;

    BaseShipModel shipModel;
    LaserWeapon laserWeapon;
    RocketLauncher rocketLauncher;

    private void Start()
    {
        upgrades = new RectTransform[] { shipHealth, shipSpeed, shipEnergy, laserDamage, laserFireRate, rocketDamage, rocketFireRate, rocketRadius, rocketAgility, rocketLifeTime };

        minusButtons = new Button[10];
        plusButtons = new Button[10];
        bars = new Image[10];

        for(int i = 0; i < upgrades.Length; i++)
        {
            minusButtons[i] = upgrades[i].Find("Minus").GetComponent<Button>();
            minusButtons[i].interactable = false;
            plusButtons[i] = upgrades[i].Find("Plus").GetComponent<Button>();
            bars[i] = upgrades[i].GetChild(0).GetChild(0).GetComponent<Image>();
            bars[i].fillAmount = 0;
        }

        ChangeShip(0);
    }

    public void ChangeShip(int value)
    {
        Master.Player = Instantiate(Master.playerPrefabs[value]);
        OnShipChanged();   
    }

    void OnShipChanged()
    {
        shipModel = Master.Player.GetComponent<BaseShipModel>();
        laserWeapon = Master.Player.GetComponent<LaserWeapon>();
        rocketLauncher = Master.Player.GetComponent<RocketLauncher>();

        UpdateValue(shipModel.HealthUpgrades, 0, minusButtons[0], plusButtons[0], bars[0]);
        UpdateValue(shipModel.SpeedUpgrades, 0, minusButtons[1], plusButtons[1], bars[1]);
        UpdateValue(shipModel.EnergyUpgrades, 0, minusButtons[2], plusButtons[2], bars[2]);
        UpdateValue(laserWeapon.DamageUpgrades, 0, minusButtons[3], plusButtons[3], bars[3]);
        UpdateValue(laserWeapon.FireRateUpgrades, 0, minusButtons[4], plusButtons[4], bars[4]);
        UpdateValue(rocketLauncher.DamageUpgrades, 0, minusButtons[5], plusButtons[5], bars[5]);
        UpdateValue(rocketLauncher.FireRateUpgrades, 0, minusButtons[6], plusButtons[6], bars[6]);
        UpdateValue(rocketLauncher.ExplosionRadiusUpgrades, 0, minusButtons[7], plusButtons[7], bars[7]);
        UpdateValue(rocketLauncher.RocketAgilityUpgrades, 0, minusButtons[8], plusButtons[8], bars[8]);
        UpdateValue(rocketLauncher.RocketLifeTimeUpgrades, 0, minusButtons[9], plusButtons[9], bars[9]);
    }

    uint UpdateValue(uint value, int scale, Button minus, Button plus, Image bar)
    {
        value = (uint)(value + scale);

        if (value == 0)
        {
            plus.interactable = true;
            minus.interactable = false;
        }
        else if (value == 5)
        {
            minus.interactable = true;
            plus.interactable = false;
        }
        else
        {
            minus.interactable = true;
            plus.interactable = true;
        }

        bar.fillAmount = (float)value / 5;

        shipModel.ApplyUpgrades();
        laserWeapon.ApplyUpgrades();
        rocketLauncher.ApplyUpgrades();

        return value;
    }

    public void ShipHealthUpgrade(int scale)
        => shipModel.HealthUpgrades = UpdateValue(shipModel.HealthUpgrades, scale, minusButtons[0], plusButtons[0], bars[0]);

    public void ShipSpeedUpgrade(int scale)
        => shipModel.SpeedUpgrades = UpdateValue(shipModel.SpeedUpgrades, scale, minusButtons[1], plusButtons[1], bars[1]);

    public void ShipEnergyUpgrade(int scale)
        => shipModel.EnergyUpgrades = UpdateValue(shipModel.EnergyUpgrades, scale, minusButtons[2], plusButtons[2], bars[2]);

    public void LaserDamageUpgrade(int scale)
        => laserWeapon.DamageUpgrades = UpdateValue(laserWeapon.DamageUpgrades, scale, minusButtons[3], plusButtons[3], bars[3]);

    public void LaserFireRateUpgrade(int scale)
        => laserWeapon.FireRateUpgrades = UpdateValue(laserWeapon.FireRateUpgrades, scale, minusButtons[4], plusButtons[4], bars[4]);

    public void RocketDamage(int scale)
        => rocketLauncher.DamageUpgrades = UpdateValue(rocketLauncher.DamageUpgrades, scale, minusButtons[5], plusButtons[5], bars[5]);

    public void RocketFireRate(int scale)
        => rocketLauncher.FireRateUpgrades = UpdateValue(rocketLauncher.FireRateUpgrades,scale, minusButtons[6], plusButtons[6], bars[6]);

    public void RocketRadius(int scale)
        => rocketLauncher.ExplosionRadiusUpgrades = UpdateValue(rocketLauncher.ExplosionRadiusUpgrades, scale, minusButtons[7], plusButtons[7], bars[7]);

    public void RocketAgility(int scale)
        => rocketLauncher.RocketAgilityUpgrades = UpdateValue(rocketLauncher.RocketAgilityUpgrades, scale, minusButtons[8], plusButtons[8], bars[8]);

    public void RocketLifetime(int scale)
        => rocketLauncher.RocketLifeTimeUpgrades = UpdateValue(rocketLauncher.RocketLifeTimeUpgrades, scale, minusButtons[9], plusButtons[9], bars[9]);
}
