﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    [SerializeField]
    UnityEngine.UI.Image image;
    public void SetProgress(float t)
        => image.fillAmount = t;

    public void StartLoading()
    {
        image.fillAmount = 0;
        image.gameObject.SetActive(true);
    }

    public void StopLoading()
    {
        image.fillAmount = 1;
        image.gameObject.SetActive(false);
    }

    public void SetLoadingProgress(float t)
    {
        image.fillAmount = t;
    }
}
