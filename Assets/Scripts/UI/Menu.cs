﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public Vector3 CameraPosition;
    public Vector3 CameraEulerAngles;

    public Quaternion CameraRotation
        => Quaternion.Euler(CameraEulerAngles);

    public void OnEnable()
    {
        
    }
}
