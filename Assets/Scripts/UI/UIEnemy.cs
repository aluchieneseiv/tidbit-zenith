﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEnemy : MonoBehaviour
{
    public GameObject Target;
    [SerializeField]
    private BarController health;
    private ShipModel targetModel;
    [SerializeField]
    private UnityEngine.UI.Text enemyName;
    [SerializeField]
    private float distanceThreshold;
    [SerializeField]
    private GameObject OnScreen;
    [SerializeField]
    private GameObject OffScreen;
    [SerializeField]
    private GameObject Close;

    private void Start()
    {
        targetModel = Target.GetComponent<ShipModel>();

        enemyName.text = Target.name;
    }

    void Update()
    {
        if (Target == null)
        {
            Destroy(gameObject);
            return;
        }

        health.Fill(targetModel.HealthFraction, targetModel.Health);

        var pos = GameMaster.Master.WorldToHudPos(Target.transform.position);
        var depth = pos.z;
        pos.Scale(Vector3.right + Vector3.up);

        if (depth > 0)
        {
            var p = pos;
            if (GameMaster.Master.ClampToCircle(ref p))
            {
                SetOnScreen(true);
                transform.localPosition = p;
                transform.localRotation = Quaternion.identity;

                var dist = GameMaster.Master.DistanceTo(Target.transform.position);

                if (dist < distanceThreshold)
                {
                    SetClose(true);
                    transform.localScale = Vector3.one * 50 / dist;
                }
                else
                {
                    SetClose(false);
                    transform.localScale = Vector3.one * 50 / (Mathf.Log(dist, distanceThreshold) * distanceThreshold);
                }
            }
            else
            {
                SetOnScreen(false);
                transform.localPosition = p;
                transform.localScale = Vector3.one * 0.6f;

                transform.localRotation = Quaternion.LookRotation(Vector3.forward, pos - p);
            }
        }
        else
        {
            SetOnScreen(false);
            transform.localScale = Vector3.one * 0.6f;

            pos = -pos;

            transform.localPosition = GameMaster.Master.CircleEdge(pos);
            transform.localRotation = Quaternion.LookRotation(Vector3.forward, pos);
        }
    }

    private void SetOnScreen(bool b)
    {
        OnScreen.SetActive(b);
        OffScreen.SetActive(!b);
    }

    private void SetClose(bool b)
    {
        Close.SetActive(b);
    }
}
