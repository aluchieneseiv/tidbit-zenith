﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour
{
    bool levelCompleted;

    public void LevelCompleted()
    {
        levelCompleted = true;
        GetComponent<MeshRenderer>().material.SetColor("_Color", Color.blue);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!levelCompleted)
            return;

        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            SceneProxy.ProxyScript.Store(GameMaster.Master.Player, "Player");
            Camera.main.gameObject.transform.parent = null;
            SceneProxy.ProxyScript.ChangeScene("Game");
        }
    }
}
