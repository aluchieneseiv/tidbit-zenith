﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerModel : MonoBehaviour
{
    [Header("Constants")]
    [SerializeField]
    private float chargeTime = 4f;
    [SerializeField]
    private float scanningDistance = 300f;
    [SerializeField]
    public LayerMask ScanningLayer;

    public uint chargeTimeUpgrades;
    public uint scanningDistanceUpgrades;

    public float ChargeTime { get; private set; }
    public float ScanningDistance { get; private set; }

    public void ApplyUpgrades()
    {
        ChargeTime = chargeTime * Mathf.Pow(0.7f, chargeTimeUpgrades);
        ScanningDistance = scanningDistance * Mathf.Pow(2f, scanningDistanceUpgrades);
    }

    private void Start()
    {
        ApplyUpgrades();
    }
}
