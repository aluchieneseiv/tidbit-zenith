﻿using UnityEngine;

public class CameraJitter : MonoBehaviour
{
    [Range(0, 1)]
    public float DefaultScanLineJitter;
    [Range(0, 1)]
    public float DefaultColorDrift;

    [HideInInspector, Range(0, 1)]
    float scanLineJitter = 0;
    public float ScanLineJitter
    {
        get
        {
            return scanLineJitter;
        }
        set
        {
            scanLineJitter = Mathf.Clamp01(value);
        }
    }
    [HideInInspector, Range(0, 1)]
    float colorDrift = 0;
    public float ColorDrift
    {
        get
        {
            return colorDrift;
        }
        set
        {
            colorDrift = Mathf.Clamp01(value);
        }
    }

    [SerializeField]
    Shader shader;
    Material material;

    float verticalJumpTime;

    private void Start()
    {
        material = new Material(shader)
        {
            hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild
        };

        ScanLineJitter = DefaultScanLineJitter;
        ColorDrift = DefaultColorDrift;
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        var sl_thresh = Mathf.Clamp01(1.0f - scanLineJitter * 1.2f);
        var sl_disp = 0.002f + Mathf.Pow(scanLineJitter, 3) * 0.05f;
        material.SetVector("_ScanLineJitter", new Vector2(sl_disp, sl_thresh));

        var cd = new Vector2(colorDrift * 0.04f, Time.time * 606.11f);
        material.SetVector("_ColorDrift", cd);

        Graphics.Blit(source, destination, material);
    }
}
