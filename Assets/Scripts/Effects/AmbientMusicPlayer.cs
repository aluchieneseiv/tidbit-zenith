﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientMusicPlayer : MonoBehaviour
{
    private AudioSource source;

    [SerializeField]
    private float volume = 0.25f;
    [SerializeField]
    private float transitionTime = 2f;
    [SerializeField]
    private AudioClip[] ambientSoundtrack;
    [SerializeField]
    private AudioClip[] combatSoundtrack;

    void Start()
    {
        source = GetComponent<AudioSource>();

        GameMaster.Master.OnEnterCombat += delegate
        {
            PlaySoundtrack(combatSoundtrack);
        };

        GameMaster.Master.OnExitCombat += delegate
        {
            PlaySoundtrack(ambientSoundtrack);
        };

        GameMaster.Master.OnPause += delegate
        {
            source.Pause();
        };

        GameMaster.Master.OnUnpause += delegate
        {
            source.UnPause();
        };

        PlaySoundtrack(ambientSoundtrack);
    }

    private Coroutine playCoroutine;
    public void PlaySoundtrack(AudioClip[] soundTrack)
    {
        if (playCoroutine != null)
            StopCoroutine(playCoroutine);

        playCoroutine = StartCoroutine(transitionMusicTo(soundTrack));
    }

    private IEnumerator transitionMusicTo(AudioClip[] soundTrack)
    {
        if (source.isPlaying)
        {
            var vol = source.volume;
            for (var t = 0f; t < transitionTime; t += Time.fixedDeltaTime)
            {
                source.volume = Mathf.Lerp(vol, 0, t);
                yield return new WaitForFixedUpdate();
            }
        }

        while (true)
        {
            source.volume = 0;

            source.clip = soundTrack[Random.Range(0, soundTrack.Length)];
            source.Play();

            for (var t = 0f; t < transitionTime; t += Time.fixedDeltaTime)
            {
                source.volume = Mathf.Lerp(0, volume, t);
                yield return new WaitForFixedUpdate();
            }

            yield return new WaitWhile(() => source.time < source.clip.length - 0.1f);
        }
    }
}
