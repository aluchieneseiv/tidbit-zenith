﻿using System;


[Flags]
public enum CinematicMode
{
    None = 0x0,
    FreezeControls,
    NoHUD
}
