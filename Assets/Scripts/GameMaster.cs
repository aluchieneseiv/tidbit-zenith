﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    [SerializeField]
    private new Camera camera;
    private CameraJitter cameraJitter;

    [Header("Prefabs")]
    [SerializeField]
    private GameObject debugPlayerPrefab;
    [SerializeField]
    private GameObject debugEnemyPrefab;
    [SerializeField]
    private GameObject debugMapPrefab;

    [Header("HUD")]
    [SerializeField]
    private HUDController hudController;

    [SerializeField]
    private GameObject pauseMenu;

    public GameObject Player { get; private set; }
    public GameObject Map { get; private set; }


    private PlayerShipController playerShipController;
    private ShipController shipController;
    private ShipModel shipModel;
    private ScannerModel scannerModel;

    private List<GameObject> enemies = new List<GameObject>();
    private uint scannedFiles;
    private uint totalFiles;

    public static GameMaster Master { get; private set; }

    Coroutine damageTakenCoroutine;

    public bool Paused
    {
        set
        {
            if (value)
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;

                shipController.StopShootingPrimary();
                shipController.StopShootingSecondary();

                playerShipController.enabled = false;
                HudEnabled = false;
                Time.timeScale = 0;
                pauseMenu.SetActive(true);

                OnPause?.Invoke();
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                playerShipController.enabled = true;
                HudEnabled = true;
                Time.timeScale = 1;
                pauseMenu.SetActive(false);

                OnUnpause?.Invoke();
            }
        }

        get
        {
            return pauseMenu.activeSelf;
        }
    }

    public bool HudEnabled
    {
        set
        {
            hudController.gameObject.SetActive(value);
        }
        get
        {
            return hudController.gameObject.activeSelf;
        }
    }

    public Vector3 NormalizedAim
        => hudController.NormalizedAim;

    public Vector3 AimDirection
        => (camera.ScreenToWorldPoint(hudController.CrosshairPos + Vector3.forward) - camera.transform.position).normalized;

    public event System.Action OnEnterCombat;
    public event System.Action OnExitCombat;

    public event System.Action OnPause;
    public event System.Action OnUnpause;

    public event System.Action OnLevelCompleted;

    public Vector3 WorldToHudPos(Vector3 pos)
        => hudController.transform.InverseTransformPoint(camera.WorldToScreenPoint(pos));

    public float DistanceTo(Vector3 pos)
        => (pos - Player.transform.position).magnitude;

    public bool ClampToCircle(ref Vector3 pos)
        => hudController.ClampToCircle(ref pos);

    public Vector3 CircleEdge(Vector3 dir)
        => hudController.CircleEdge(dir);

    public void AddEnemy(GameObject gameObject)
    {
        var ai = gameObject.GetComponent<AIController>();

        if (ai != null)
            ai.Target = Player.transform;

        enemies.Add(gameObject);

        hudController.AddEnemy(gameObject);
    }

    private void Awake()
    {
        Master = this;
        OnLevelCompleted = null;
    }

    private void Start()
    {
        if (!SceneProxy.ProxyExists)
        {
            debug();
        }
        else
        {
            Player = SceneProxy.ProxyScript.Extract("Player");
        }

        StartCoroutine(spawnCoroutine());

        StartCoroutine(monitorCombat());

        cameraJitter = camera.GetComponent<CameraJitter>();
    }

    private IEnumerator monitorCombat()
    {
        var combat = false;
        while (true)
        {
            if (!combat && enemies.Count > 0)
            {
                combat = true;
                OnEnterCombat?.Invoke();
            }
            else if (combat && enemies.Count == 0)
            {
                combat = false;
                OnExitCombat?.Invoke();
            }

            yield return new WaitForSeconds(3f);
        }
    }

    void debug()
    {
        Map = Instantiate(debugMapPrefab, Vector3.zero, Quaternion.identity);
        Map.name = "Map (Debug)";

        Player = Instantiate(debugPlayerPrefab, Vector3.zero, Quaternion.identity);
        Player.name = "Player (Debug)";

        var enemy = Instantiate(debugEnemyPrefab, Vector3.one * 100, Quaternion.identity);
        enemy.name = "Enemy (Debug)";

        AddEnemy(enemy);
    }

    IEnumerator spawnCoroutine()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Time.timeScale = 0;

        Player.transform.position = Random.onUnitSphere * 9900f;
        Player.transform.rotation = Quaternion.LookRotation(-Player.transform.position);

        camera.transform.SetParent(Player.transform, false);
        camera.gameObject.AddComponent<CameraController>();

        shipModel = Player.GetComponent<ShipModel>();
        scannerModel = Player.GetComponent<ScannerModel>();

        shipModel.OnKilled += onDeath;

        shipController = Player.AddComponent<ShipController>();

        yield return new WaitForSecondsRealtime(0.3f);

        Time.timeScale = 1;

        playerShipController = Player.AddComponent<PlayerShipController>();
        HudEnabled = true;
        IsScanning = false;

        yield break;
    }

    private void onDeath()
    {
        StartCoroutine(deathCoroutine());
    }

    IEnumerator deathCoroutine()
    {
        Destroy(camera.GetComponent<CameraController>());
        camera.transform.SetParent(null, true);
        Destroy(Player);

        if (SceneProxy.ProxyExists)
        {
            Announce("Unfortunate...");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            yield return new WaitForSeconds(2);

            SceneProxy.ProxyScript.ChangeScene("Menu");
        }
        else
        {
            Destroy(Map);
            foreach (var enemy in enemies)
                Destroy(enemy);

            enemies.Clear();

            debug();
            Announce("Debug: Respawning...", 1);
            StartCoroutine(spawnCoroutine());
        }
        yield break;

    }

    public void DamageTaken()
    {
        if (damageTakenCoroutine != null)
            StopCoroutine(damageTakenCoroutine);
        damageTakenCoroutine = StartCoroutine(_DamageTaken(0.25f));
    }

    IEnumerator _DamageTaken(float duration)
    {
        for (float i = 0; i <= 1; i += 0.1f)
        {
            cameraJitter.ScanLineJitter = Mathf.Lerp(0.3f, 0, i);
            cameraJitter.ColorDrift = Mathf.Lerp(0.3f, 0, i);

            yield return new WaitForSeconds(duration / 10);
        }

        cameraJitter.ScanLineJitter = cameraJitter.DefaultScanLineJitter;
        cameraJitter.ColorDrift = cameraJitter.DefaultColorDrift;
    }

    private void Update()
    {
        if (Player != null)
        {
            hudController.UpdateHealth(shipModel.HealthFraction);
            hudController.UpdateEnergy(shipModel.EnergyFraction);

            if (Input.GetKeyDown(KeyCode.Escape))
                Paused = !Paused;
        }
    }

    public void SetFileStats(uint scannedFiles, uint totalFiles)
    {
        this.scannedFiles = scannedFiles;
        this.totalFiles = totalFiles;

        if (scannedFiles == totalFiles)
        {
            if (enemies.Count == 0)
                OnLevelCompleted?.Invoke();
            else
                OnExitCombat += delegate { OnLevelCompleted?.Invoke(); };
        }
    }

    public bool IsScanning { get; private set; }

    public IEnumerator Scan(Generation.Object obj)
    {
        if (obj.Scanned)
        {
            Announce("File is clean!");
            yield break;
        }

        IsScanning = true;
        hudController.SetScanner(0);
        hudController.SetScannerActive(true);

        for (var t = 0f; t < scannerModel.ChargeTime; t += Time.fixedDeltaTime)
        {
            hudController.SetScanner(Mathf.InverseLerp(0, scannerModel.ChargeTime, t));
            yield return new WaitForFixedUpdate();
        }

        hudController.SetScannerActive(false);

        if (!obj.threatModel.HasValue)
            Announce("File is clean!");
        else
        {
            var pos = obj.transform.position;
            var threat = obj.threatModel.Value;

            Destroy(obj.gameObject);

            for (int i = 0; i < threat.count; i++)
            {
                var enemy = Instantiate(threat.prefab, pos, Random.rotation);

                enemy.name = threat.prefab.name;

                AddEnemy(enemy);
            }
        }

        SetFileStats(scannedFiles + 1, totalFiles);
        obj.Scanned = true;
        IsScanning = false;
    }

    private void FixedUpdate()
    {
        enemies.RemoveAll(obj => obj == null);
    }

    public void Announce(string text, float time = 3f, float speed = 60f)
        => hudController.Announce(text, speed, time);
}
