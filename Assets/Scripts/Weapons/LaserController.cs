﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    private float Damage
    {
        get
        {
            LaserWeapon w;
            if(weapon.TryGetTarget(out w))
            {
                return w.Damage;
            }

            return 0;
        }
    }

    private ParticleSystem psystem;
    private AudioSource source;
    private System.WeakReference<LaserWeapon> weapon;
    void Awake()
    {
        psystem = GetComponent<ParticleSystem>();
        source = GetComponent<AudioSource>();
    }

    public void Init(LaserWeapon laserWeapon, LayerMask layer)
    {
        var col = psystem.collision;
        col.collidesWith = layer;

        laserWeapon.bulletSpeed = psystem.main.startSpeed.constant;

        weapon = new System.WeakReference<LaserWeapon>(laserWeapon);
    }

    // Update is called once per frame
    public void Shoot()
    {
        psystem.Emit(1);
        source.Play();
    }

    private void OnParticleCollision(GameObject other)
    {
        other.SendMessageUpwards("TriggerDamage", Damage, SendMessageOptions.DontRequireReceiver);
    }
}
