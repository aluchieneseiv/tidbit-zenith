﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : SecondaryWeapon
{
    [Header("Constants")]
    [SerializeField]
    private float fireRate = 0.5f;
    [SerializeField]
    private float energyUse = 15f;
    [SerializeField]
    private float damage = 20f;
    [SerializeField]
    private float explosionRadius = 25f;
    [SerializeField]
    private float rocketSpeed = 250f;
    [SerializeField]
    private float rocketRotationSpeed = 50f;
    [SerializeField]
    private float rocketLifeTime = 10f;
    [SerializeField]
    private float homingFOV = 100;
    [SerializeField]
    private LayerMask homingTargetLayer;

    [Header("Upgrades")]
    public uint DamageUpgrades = 0;
    public uint FireRateUpgrades = 0;
    public uint ExplosionRadiusUpgrades = 0;
    public uint RocketAgilityUpgrades = 0;
    public uint RocketLifeTimeUpgrades = 0;

    public float Damage { get; private set; }
    public float EnergyUse { get; private set; }
    public float FireRate { get; private set; }
    public float ExplosionRadius { get; private set; }
    public float RocketSpeed { get; private set; }
    public float RocketRotationSpeed { get; private set; }
    public float RocketLifeTime { get; private set; }
    public GameObject ExplosionPrefab { get; private set; }

    private GameObject rocketPrefab;
    private GameObject[] launchers;
    private Coroutine shootingCoroutine;
    private bool shooting;
    private new Collider collider;
    private new Rigidbody rigidbody;

    public override void ApplyUpgrades()
    {
        Damage = damage * Mathf.Pow(1.25f, DamageUpgrades);
        FireRate = fireRate * Mathf.Pow(1.25f, FireRateUpgrades);
        EnergyUse = energyUse;
        ExplosionRadius = explosionRadius * Mathf.Pow(2.25f, ExplosionRadiusUpgrades);
        RocketSpeed = rocketSpeed * Mathf.Pow(2.25f, RocketAgilityUpgrades);
        RocketRotationSpeed = rocketRotationSpeed * Mathf.Pow(2.25f, RocketAgilityUpgrades);
        RocketLifeTime = rocketLifeTime * Mathf.Pow(2.25f, RocketLifeTimeUpgrades);
    }

    public override void Init(GameObject[] weapons)
    {
        shipModel = GetComponent<ShipModel>();

        rocketPrefab = Resources.Load("Rocket") as GameObject;
        ExplosionPrefab = Resources.Load("RocketExplosion") as GameObject;

        launchers = weapons;

        rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();

        ApplyUpgrades();
    }

    protected IEnumerator Shoot()
    {
        while (shooting)
        {
            if (!shipModel.TryUseEnergy(EnergyUse * launchers.Length))
                break;

            Transform homingTarget = null;
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, homingFOV, transform.forward, out hit, RocketSpeed * RocketLifeTime, homingTargetLayer))
                homingTarget = hit.transform;

            foreach (var l in launchers)
            {
                var r = Instantiate(rocketPrefab, l.transform.position, transform.rotation);
                Collider c = r.GetComponent<Collider>();
                Physics.IgnoreCollision(c, collider);
                StartCoroutine(ResetCollision(c));
                r.GetComponent<Rigidbody>().velocity = rigidbody.velocity;
                r.GetComponent<RocketController>().Init(this, Target, homingTarget);
            }

            yield return new WaitForSeconds(1 / FireRate);
        }
        shootingCoroutine = null;
    }

    IEnumerator ResetCollision(Collider c)
    {
        yield return new WaitForSeconds(0.5f);

        Physics.IgnoreCollision(c, collider, false);
    }

    public override void StartShooting()
    {
        if (shootingCoroutine == null)
        {
            shooting = true;
            shootingCoroutine = StartCoroutine(Shoot());
        }
    }

    public override void StopShooting()
    {
        shooting = false;
    }
}
