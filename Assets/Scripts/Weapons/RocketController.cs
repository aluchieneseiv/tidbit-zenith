﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    private float damage;
    private float radius;
    private float speed;
    private float rspeed;
    private Transform target;
    private LayerMask layerMask;
    private new Rigidbody rigidbody;
    private GameObject explosionPrefab;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        GetComponentInChildren<Thruster>().Scale(1);
    }

    public void Init(RocketLauncher rocketLauncher, LayerMask layer, Transform target)
    {
        damage = rocketLauncher.Damage;
        radius = rocketLauncher.ExplosionRadius;
        speed = rocketLauncher.RocketSpeed;
        rspeed = rocketLauncher.RocketRotationSpeed;
        explosionPrefab = rocketLauncher.ExplosionPrefab;

        layerMask = layer;

        this.target = target;

        StartCoroutine(ExplodeTimer(rocketLauncher.RocketLifeTime));
    }

    private void FixedUpdate()
    {
        rigidbody.AddRelativeForce(Vector3.forward * speed * rigidbody.drag, ForceMode.Acceleration);

        if (target == null)
            return;

        var seek_dir = transform.InverseTransformDirection(target.position - transform.position).normalized;
        var rotate_dir = Vector3.Cross(Vector3.forward, seek_dir);

        rigidbody.AddRelativeTorque(rotate_dir * rspeed * rigidbody.angularDrag, ForceMode.Acceleration);
    }

    IEnumerator ExplodeTimer(float time)
    {
        yield return new WaitForSeconds(time);
        Explode();
    }

    void Explode()
    {
        foreach(var c in Physics.OverlapSphere(transform.position, radius, layerMask))
            c.gameObject.SendMessageUpwards("TriggerDamage", damage, SendMessageOptions.DontRequireReceiver);
        Destroy(Instantiate(explosionPrefab, transform.position, Quaternion.identity), 3f);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }
}
