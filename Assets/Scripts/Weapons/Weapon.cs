﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public LayerMask Target;
    protected ShipModel shipModel;

    public abstract void ApplyUpgrades();

    public abstract void Init(GameObject[] weapons);

    private void Start()
    {
        shipModel = GetComponent<ShipModel>();
    }

    public abstract void StartShooting();

    public abstract void StopShooting();
}

public abstract class PrimaryWeapon : Weapon
{

}

public abstract class SecondaryWeapon : Weapon
{

}
