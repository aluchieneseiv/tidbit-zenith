﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LaserWeapon : PrimaryWeapon
{
    [Header("Constants")]
    [SerializeField]
    private float fireRate = 10f;
    [SerializeField]
    private float energyUse = 0.4f;
    [SerializeField]
    private float damage = 2f;

    [HideInInspector]
    public float bulletSpeed;

    [Header("Upgrades")]
    public uint DamageUpgrades = 0;
    public uint FireRateUpgrades = 0;

    public float Damage { get; private set; }
    public float EnergyUse { get; private set; }
    public float FireRate { get; private set; }

    private GameObject weaponPrefab;
    private LaserController[] controllers;
    private Coroutine shootingCoroutine;
    private bool shooting;

    public override void ApplyUpgrades()
    {
        Damage = damage * Mathf.Pow(1.25f, DamageUpgrades);
        FireRate = fireRate * Mathf.Pow(1.25f, FireRateUpgrades);
        EnergyUse = energyUse;
    }

    public override void Init(GameObject[] weapons)
    {
        shipModel = GetComponent<ShipModel>();

        weaponPrefab = Resources.Load("LaserShooter") as GameObject;

        controllers = System.Array.ConvertAll(weapons, delegate (GameObject gun)
        {
            var w = Instantiate(weaponPrefab, gun.transform).GetComponent<LaserController>();
            w.Init(this, Target);

            return w;
        });

        ApplyUpgrades();
    }

    protected IEnumerator Shoot()
    {
        while (shooting)
        {
            if (!shipModel.TryUseEnergy(EnergyUse * controllers.Length))
                break;

            foreach (var c in controllers)
                c.Shoot();

            yield return new WaitForSeconds(1 / FireRate);
        }
        shootingCoroutine = null;
    }

    public override void StartShooting()
    {
        if (shootingCoroutine == null)
        {
            shooting = true;
            shootingCoroutine = StartCoroutine(Shoot());
        }
    }

    public override void StopShooting()
    {
        shooting = false;
    }
}
