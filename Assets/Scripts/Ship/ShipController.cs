﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShipController : MonoBehaviour
{
    [SerializeField]
    [Range(1f, 5f)]
    private int physicsPrecision = 4;

    [SerializeField]
    private float engineVolume = 0.1f;

    private new Rigidbody rigidbody;
    private ShipModel shipModel;
    private AudioSource source;

    public bool IsTurboing
        => rigidbody.velocity.magnitude > shipModel.MaxForwardSpeed + shipModel.MaxStrafeSpeed;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.sleepThreshold = Mathf.Pow(0.005f, physicsPrecision);
        shipModel = GetComponent<ShipModel>();
        source = GetComponent<AudioSource>();

        source.volume = 0;
        source.Play();
    }

    public void Rotate(Vector3 torque)
        => rigidbody.AddRelativeTorque(torque * shipModel.MaxRotateSpeed * rigidbody.angularDrag, ForceMode.Acceleration);

    public void Accelerate(float scale)
    {
        if (scale >= 0)
            rigidbody.AddRelativeForce(Vector3.forward * scale * shipModel.MaxForwardSpeed * rigidbody.drag * shipModel.SpeedMultiplier, ForceMode.Acceleration);
        else
            rigidbody.AddRelativeForce(scale * Vector3.forward * shipModel.MaxStrafeSpeed * rigidbody.drag, ForceMode.Acceleration);

        var tscale = Mathf.Clamp01(scale);
        SetThrusterPower(tscale);
    }

    public void Turbo()
    {
        if (shipModel.TryUseEnergy(shipModel.TurboEnergyUse * Time.fixedDeltaTime))
        {
            rigidbody.AddRelativeForce(Vector3.forward * shipModel.MaxForwardSpeed * rigidbody.drag * shipModel.TurboMultiplier * shipModel.SpeedMultiplier, ForceMode.Acceleration);
            SetThrusterPower(Mathf.InverseLerp(0, shipModel.MaxForwardSpeed, Vector3.Dot(rigidbody.velocity, transform.forward)));
        }
    }

    public void Strafe(Vector3 dir)
        => rigidbody.AddRelativeForce(dir * shipModel.MaxStrafeSpeed * rigidbody.drag, ForceMode.Acceleration);

    public void SetThrusterPower(float len)
    {
        shipModel.Thruster.Scale(len);
        source.volume = Mathf.Lerp(0, engineVolume, len);
    }

    public void StartShootingPrimary()
    {
        if (!IsTurboing)
            shipModel.PrimaryWeapon?.StartShooting();
    }

    public void StopShootingPrimary()
        => shipModel.PrimaryWeapon?.StopShooting();

    public void StartShootingSecondary()
    {
        if (!IsTurboing)
            shipModel.SecondaryWeapon?.StartShooting();
    }

    public void StopShootingSecondary()
        => shipModel.SecondaryWeapon?.StopShooting();

    public void SetGunDirection(Vector3 direction)
    {
        var rotation = Quaternion.LookRotation(direction, Vector3.up);

        foreach (var gun in shipModel.PrimaryWeapons)
            gun.transform.localRotation = rotation;

        /*
        foreach (var gun in secondaryWeapons)
            gun.transform.localRotation = rotation;
        */
    }

    private void Update()
    {
        if(IsTurboing)
        {
            StopShootingPrimary();
            StopShootingSecondary();
        }
    }
}
