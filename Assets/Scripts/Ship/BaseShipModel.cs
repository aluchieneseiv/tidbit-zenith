﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShipModel : MonoBehaviour {

    [Header("Constants")]
    [Range(50, 1000)]
    [SerializeField]
    private float maxForwardSpeed = 200f;
    [Range(1.5f, 5f)]
    [SerializeField]
    private float turboMultiplier = 2f;
    [Range(1f, 5f)]
    [SerializeField]
    private float turboEnergyUse = 4f;
    [Range(20, 500)]
    [SerializeField]
    private float maxStrafeSpeed = 100f;
    [Range(1f, 10f)]
    [SerializeField]
    private float maxRotateSpeed = 3f;
    [Range(0, 300)]
    [SerializeField]
    private float maxHealth = 100f;
    [Range(0.2f, 5f)]
    [SerializeField]
    private float energyUse = 1f;
    [Range(3, 100)]
    [SerializeField]
    private float energyDissipationRate = 6f;

    [Header("Upgrades")]
    public uint HealthUpgrades = 0;
    public uint SpeedUpgrades = 0;
    public uint EnergyUpgrades = 0;
    public bool TurboUsesEnergy = true;

    public float MaxForwardSpeed { get; private set; }
    public float TurboMultiplier { get; private set; }
    public float TurboEnergyUse { get; private set; }
    public float MaxStrafeSpeed { get; private set; }
    public float MaxRotateSpeed { get; private set; }
    public float MaxHealth { get; private set; }
    public float EnergyUse { get; private set; }
    public float EnergyDissipationRate { get; private set; }

    [Header("Components")]
    public GameObject[] PrimaryWeapons;
    public GameObject[] SecondaryWeapons;
    public Thruster Thruster;

    public void ApplyUpgrades()
    {
        MaxForwardSpeed = maxForwardSpeed * Mathf.Pow(1.25f, SpeedUpgrades);
        TurboMultiplier = turboMultiplier;
        TurboEnergyUse = TurboUsesEnergy ? turboEnergyUse : 0;
        MaxStrafeSpeed = maxStrafeSpeed * Mathf.Pow(1.20f, SpeedUpgrades);
        MaxRotateSpeed = maxRotateSpeed;
        MaxHealth = maxHealth * Mathf.Pow(1.4f, HealthUpgrades);
        EnergyUse = energyUse * Mathf.Pow(0.8f, EnergyUpgrades);
        EnergyDissipationRate = energyDissipationRate * Mathf.Pow(1.15f, EnergyUpgrades);
    }

    public void Start()
    {

    }

}
