﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ShipModel : MonoBehaviour
{
    private BaseShipModel baseShipModel;

    public float Health { get; private set; }
    public float Energy { get; private set; }
    public float HealthFraction
        => Health / MaxHealth;
    public float EnergyFraction
        => Energy / MaxEnergy;

    public float MaxForwardSpeed
        => baseShipModel.MaxForwardSpeed * SpeedMultiplier;

    public float TurboMultiplier
        => baseShipModel.TurboMultiplier;

    public float TurboEnergyUse
        => baseShipModel.TurboEnergyUse;

    public float MaxStrafeSpeed
        => baseShipModel.MaxStrafeSpeed * SpeedMultiplier;

    public float MaxRotateSpeed
        => baseShipModel.MaxRotateSpeed;

    public float MaxHealth
        => baseShipModel.MaxHealth;

    public readonly float MaxEnergy = 100f;

    public float EnergyUse
        => baseShipModel.EnergyUse * EnergyUseMultiplier;

    public float EnergyDissipationRate
        => baseShipModel.EnergyDissipationRate * EnergyDissipationMultiplier;

    public event Action<float> OnDamage;
    public event Action<float> OnUseEnergy;
    public event Action OnKilled;

    [Header("Multipliers")]
    public float SpeedMultiplier = 1f;
    public float EnergyUseMultiplier = 1f;
    public float EnergyDissipationMultiplier = 1f;
    public float DamageTakenMultiplier = 1f;

    private PrimaryWeapon primaryWeapon;
    private SecondaryWeapon secondaryWeapon;

    public PrimaryWeapon PrimaryWeapon
        => primaryWeapon;

    public SecondaryWeapon SecondaryWeapon
        => secondaryWeapon;

    public GameObject[] PrimaryWeapons
        => baseShipModel.PrimaryWeapons;

    public Thruster Thruster => baseShipModel.Thruster;

    private GameObject explosionPrefab;

    private void Start()
    {
        baseShipModel = GetComponent<BaseShipModel>();
        primaryWeapon = GetComponent<PrimaryWeapon>();
        secondaryWeapon = GetComponent<SecondaryWeapon>();

        baseShipModel.ApplyUpgrades();
        primaryWeapon?.ApplyUpgrades();
        secondaryWeapon?.ApplyUpgrades();

        primaryWeapon?.Init(baseShipModel.PrimaryWeapons);
        secondaryWeapon?.Init(baseShipModel.SecondaryWeapons);

        explosionPrefab = Resources.Load("Explosion") as GameObject;
        OnKilled += delegate { Destroy(Instantiate(explosionPrefab, transform.position, Quaternion.identity), 3f); };

        ResetMultipliers();
    }

    private void FixedUpdate()
    {
        Energy -= EnergyDissipationRate * Time.fixedDeltaTime;

        if (Energy < 0)
            Energy = 0;
    }

    public void ResetMultipliers()
    {
        Energy = 0;
        Health = MaxHealth;

        RemoveEffects();
    }

    private List<IEnumerator> effects = new List<IEnumerator>();

    public Coroutine ApplyEffect(IEnumerator effectCoroutine)
    {
        effects.Add(effectCoroutine);
        return StartCoroutine(effectCoroutine);
    }

    public void RemoveEffects()
    {
        SpeedMultiplier = 1f;
        EnergyUseMultiplier = 1f;
        EnergyDissipationMultiplier = 1f;
        DamageTakenMultiplier = 1f;

        foreach (var coroutine in effects)
        {
            StopCoroutine(coroutine);
            (coroutine as IDisposable)?.Dispose();
        }

        effects.Clear();
    }

    public bool TryUseEnergy(float amount)
    {
        amount *= EnergyUseMultiplier;

        if (Energy + amount > MaxEnergy)
            return false;

        Energy += amount;
        OnUseEnergy?.Invoke(amount);
        return true;
    }

    public void TriggerDamage(float amount)
    {
        amount *= DamageTakenMultiplier;

        var last = Health;

        Health = Mathf.Clamp(Health - amount, 0, MaxHealth);

        if (last > Health)
            OnDamage?.Invoke(Health - last);

        if (Health == 0)
            OnKilled?.Invoke();
    }
}
