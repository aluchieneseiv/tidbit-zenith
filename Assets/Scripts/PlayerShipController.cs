﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameMaster;

public class PlayerShipController : MonoBehaviour
{
    private ShipController shipController;
    private ShipModel shipModel;
    private ScannerModel scannerModel;

    public bool FreezeControls { get; set; }

    private void Start()
    {
        print("running Start");
        shipController = GetComponent<ShipController>();

        shipModel = GetComponent<ShipModel>();

        scannerModel = GetComponent<ScannerModel>();

        shipModel.OnDamage += delegate { Master.DamageTaken(); };
    }

    private Vector3 aim;

    void FixedUpdate()
    {
        if (FreezeControls)
            return;

        aim.Set(-Master.NormalizedAim.y, Master.NormalizedAim.x, 0);

        aim = aim.normalized * Mathf.Pow(aim.magnitude, 3);

        shipController.Rotate(Vector3.Cross(aim, Vector3.forward));

        shipController.SetGunDirection(transform.InverseTransformDirection(Master.AimDirection));

        shipController.Rotate(Vector3.forward * -Input.GetAxis("Roll"));

        if (Input.GetKey(KeyCode.LeftShift))
            shipController.Turbo();
        else
            shipController.Accelerate(Input.GetAxis("Forward"));

        shipController.Strafe(Vector3.right * Input.GetAxis("Horizontal"));

        shipController.Strafe(Vector3.up * Input.GetAxis("Vertical"));
    }

    private void Update()
    {
        if (FreezeControls)
            return;

        if (Input.GetMouseButtonDown(0))
            shipController.StartShootingPrimary();
        if (Input.GetMouseButtonUp(0))
            shipController.StopShootingPrimary();

        if (Input.GetMouseButtonDown(1))
            shipController.StartShootingSecondary();
        if (Input.GetMouseButtonUp(1))
            shipController.StopShootingSecondary();

        if (Input.GetMouseButtonDown(2) && !Master.IsScanning)
        {

            RaycastHit hitInfo;
            
            if (Physics.SphereCast(transform.position, 10, Master.AimDirection, out hitInfo, scannerModel.ScanningDistance, scannerModel.ScanningLayer))
            {
                var obj = hitInfo.transform.gameObject;

                var comp = obj.GetComponent<Generation.Object>();

                if (comp != null)
                    StartCoroutine(Master.Scan(comp));
            }
        }

        Debug.DrawLine(transform.position, transform.position + 10 * Master.AimDirection);

    }
}
