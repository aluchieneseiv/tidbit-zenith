﻿Shader "Custom/EngineTrail" 
{
	Properties
	{
		_TintColor("Tint Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex("Particle Texture", 2D) = "white" {}
		_Scale("Scale", float) = 1
		_Speed("Speed", float) = 10
		_Amplitude("Amp", float) = 0.1
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		AlphaTest Greater .01
		ColorMask RGB
		Lighting Off ZWrite On ZTest LEqual Fog{ Color(0,0,0,0) }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			fixed4 _TintColor;
			sampler2D _MainTex;
			float _Scale;
			float _Speed;
			float _Amplitude;

			struct v2f 
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			float4 _MainTex_ST;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				o.texcoord.x *= _Scale;
				o.texcoord.x -= sin(_Time.y * _Speed + o.vertex.z) * _Amplitude;
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				return 4.0f * i.color * _TintColor * tex2D(_MainTex, i.texcoord);
			}
			ENDCG
		}
	}
}