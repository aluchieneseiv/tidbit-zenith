﻿Shader "Custom/Bloom" 
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	sampler2D	_MainTex;
	sampler2D	_SourceTex;
	float4		_MainTex_TexelSize;
	half4		_Filter;
	half		_Intensity;

	half3 Prefilter(half3 c) 
	{
		half brightness = max(c.r, max(c.g, c.b));
		half soft = brightness - _Filter.y;
		soft = clamp(soft, 0, _Filter.z);
		soft = soft * soft * _Filter.w;
		half contribution = max(soft, brightness - _Filter.x);
		contribution /= max(brightness, 0.00001);
		return c * contribution;
	}

	half3 Sample(float2 uv) 
	{
		return tex2D(_MainTex, uv).rgb;
	}

	half3 SampleBox(float2 uv, float delta) 
	{
		float4 o = _MainTex_TexelSize.xyxy * float2(-delta, delta).xxyy;
		half3 s =
			Sample(uv + o.xy) + Sample(uv + o.zy) +
			Sample(uv + o.xw) + Sample(uv + o.zw);
		return s / 4;
	}

	struct v2f 
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	v2f vert(appdata_base v) 
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord;
		return o;
	}
	ENDCG

	SubShader
	{
		Cull Off
		ZTest Always
		ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma	 fragment frag

			half4 frag(v2f i) : SV_Target
			{
				return half4(Prefilter(SampleBox(i.uv, 1)), 1);
			}	
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f i) : SV_Target
			{
				return half4(SampleBox(i.uv, 1), 1);
			}
			ENDCG
		}

		Pass
		{
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f i) : SV_Target
			{
				return half4(_Intensity * SampleBox(i.uv, 0.5), 1);
			}
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f i) : SV_Target
			{
				half4 c = tex2D(_SourceTex, i.uv);
				c.rgb += _Intensity * SampleBox(i.uv, 0.5);
				return c;
			}
			ENDCG
		}
	}
}