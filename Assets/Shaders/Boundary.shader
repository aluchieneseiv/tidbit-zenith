﻿Shader "Custom/Boundary" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB) Trans (A)", 2D) = "white" {}
		_GlowPower("Glow power", range(0, 10)) = 1
		_MaxVisDistance("Max Vis Distance", float) = 100
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard alpha:blend

		#pragma target 3.0

		#include "UnityStandardUtils.cginc"

		half4		_Color;
		sampler2D   _MainTex;
		half		_GlowPower;
		half        _MaxVisDistance;

		struct Input {
			float3 worldPos;
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			half4 color = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = color.rgb * _Color;
			o.Emission = color.rgb * _Color * _GlowPower;
			float distance = length(_WorldSpaceCameraPos - IN.worldPos);
			o.Alpha = (1 - saturate(distance / _MaxVisDistance)) * color.a;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
