﻿Shader "Custom/Triplanar" 
{
	Properties
	{
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_TexColor("Tex Color", Color) = (1, 1, 1, 1)
		_GlowMap("Glow Map", 2D) = "black" {}
		_GlowColor("Glow Color", Color) = (1, 1, 1, 1)
		_GlowPower("Glow", range(0, 10)) = 1
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower("Rim Power", Range(1.0, 6.0)) = 3.0
		[Toggle] _TextureMovement("Move Texture Over Time", float) = 0
		_TextureSpeed("Texture Speed", Range(0, 2)) = 0.2
		[Toggle] _VertexMovement("Move Vertices Over Time", float) = 0
		_PulseSpeed("Pulsing Speed", Range(0, 2)) = 0.5
		_PulseDistance("Max Pulsing Distance", Range(0, 0.5)) = 0.1
	}

	SubShader
	{
		Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
		LOD 200
		
		Pass {
			ZWrite On
			ColorMask 0
		}

		CGPROGRAM
		#pragma surface surf Standard alpha:blend vertex:vert

		#pragma target 3.0

		#include "UnityStandardUtils.cginc"

		sampler2D	_MainTex;
		float4		_MainTex_ST;
		half4		_TexColor;

		sampler2D	_GlowMap;
		half4		_GlowColor;
		float		_GlowPower;

		half4		_RimColor;
		float		_RimPower;

		float		_TextureMovement;
		float		_TextureSpeed;
		float		_VertexMovement;
		float		_PulseSpeed;
		float		_PulseDistance;

		struct Input 
		{
			float3 worldPos;
			float3 worldNormal;
			float3 viewDir;
			INTERNAL_DATA
		};

		void vert(inout appdata_full v)
		{
			v.vertex.xyz += _VertexMovement * v.normal.xyz * abs(sin(_Time.y * _PulseSpeed)) * _PulseDistance;
		}

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			// calculate triplanar blend
			half3 triblend = saturate(pow(IN.worldNormal, 4));
			triblend /= max(dot(triblend, half3(1,1,1)), 0.0001);

			// calculate triplanar uvs
			// applying texture scale and offset values ala TRANSFORM_TEX macro
			float offset = _MainTex_ST.yz + _TextureMovement * _Time * _TextureSpeed;
			float2 uvX = IN.worldPos.zy * _MainTex_ST.xy + offset;
			float2 uvY = IN.worldPos.xz * _MainTex_ST.xy + offset;
			float2 uvZ = IN.worldPos.xy * _MainTex_ST.xy + offset;

			// minor optimization of sign(). prevents return value of 0
			half3 axisSign = IN.worldNormal < 0 ? -1 : 1;

			// albedo textures
			fixed4 colX = tex2D(_MainTex, uvX);
			fixed4 colY = tex2D(_MainTex, uvY);
			fixed4 colZ = tex2D(_MainTex, uvZ);
			fixed4 col = colX * triblend.x + colY * triblend.y + colZ * triblend.z;
			col *= _TexColor;

			// glow textures
			fixed4 glowX = tex2D(_GlowMap, uvX);
			fixed4 glowY = tex2D(_GlowMap, uvY);
			fixed4 glowZ = tex2D(_GlowMap, uvZ);
			fixed4 glow = glowX * triblend.x + glowY * triblend.y + glowZ * triblend.z;

			o.Albedo = col.rgb;
			o.Alpha = col.a;
			o.Emission = glow * _GlowColor * _GlowPower;
		
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission += _RimColor.rgb * pow(rim, _RimPower);
		}
		ENDCG
	}
	FallBack "Diffuse"
}